#!/bin/bash

#Assign file name according to module version before execution
file=lint-output-v1.1.2
golangci-lint run -v --out-format "json:lint/$file.json,colored-line-number,tab:lint/lint-output.txt"

esc=$'\e'
sed "s/$esc\[[0-9;]*m//g" lint/lint-output.txt
sed "s/$esc\[[0-9;]*m//g" lint/lint-output.txt > lint/${file}.txt

# golangci-lint run -v --fix
# gofmt 에러 발생시 cli: gofmt -s -w ${file}.go