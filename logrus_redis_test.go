package log

import (
	"testing"
)

func TestInit(t *testing.T) {
	type config struct {
		REDIS_LOG_HOST string
		REDIS_LOG_PASSWORD string
		REDIS_LOG_PORT string
		REDIS_LOG_CONTAINER string
		DOMAIN_ID string
		SERVICE_NAME string
		ENV string
		PROJECT_OWNER_SLACK_ID string
	}
	cfg := &config{
		REDIS_LOG_HOST:         "dta-wi-redis-dev.weltcorp.com",
		REDIS_LOG_PASSWORD:     "welt10004",
		REDIS_LOG_PORT:         "6379",
		REDIS_LOG_CONTAINER:    "AAAA",
		DOMAIN_ID:              "domain_id",
		SERVICE_NAME:           "service_name",
		ENV:                    "env",
		PROJECT_OWNER_SLACK_ID: "slack_id",
	}
	type args struct {
		env interface{}
		TLS bool
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "TLS Test",
			args: args{
				env: cfg,
				TLS: true,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := Init(tt.args.env, tt.args.TLS)
			if (err != nil) != tt.wantErr {
				t.Errorf("Init() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			I("Test!")
		})
	}
}
